package com.example.nomina_kotlin

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {

    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button
    private lateinit var txtUsuario: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()

        btnIngresar.setOnClickListener {
            btnIngresar()
        }

        btnSalir.setOnClickListener {
            btnSalir()
        }
    }

    private fun iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar)
        btnSalir = findViewById(R.id.btnSalir)
        txtUsuario = findViewById(R.id.txtUsuario)
    }

    private fun btnIngresar() {
        val strNombre = getString(R.string.nombre)

        if (strNombre == txtUsuario.text.toString()) {
            val bundle = Bundle()
            bundle.putString("nombre", txtUsuario.text.toString())

            val intent = Intent(this, ReciboNominaActivity::class.java)
            intent.putExtras(bundle)

            startActivity(intent)
        } else {
            Toast.makeText(applicationContext, "El nombre no es válido", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnSalir() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo Nómina")
        confirmar.setMessage("Cerrar la aplicación?")
        confirmar.setPositiveButton("Confirmar") { dialog, _ ->
            finish()
        }
        confirmar.setNegativeButton("Cancelar") { dialog, _ ->
            dialog.dismiss()
        }
        confirmar.show()
    }
}