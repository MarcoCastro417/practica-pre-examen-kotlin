package com.example.nomina_kotlin

class ReciboNomina(
    var numRecibo: Int,
    var nombre: String,
    var horasTrabNormal: Float,
    var horasTrabExtras: Float,
    var puesto: Int,
    var impuestoPorc: Float
) {
    // Métodos para cálculos

    fun reciboNominal(): Float {
        val pagoBase = 200f
        return when (puesto) {
            1 -> pagoBase * 1.2f
            2 -> pagoBase * 1.5f
            3 -> pagoBase * 2.0f
            else -> pagoBase
        }
    }

    fun calcularSubtotal(): Float {
        val pagoBase = reciboNominal()
        val pagoHoraExtra = pagoBase * 2
        return (pagoBase * horasTrabNormal) + (pagoHoraExtra * horasTrabExtras)
    }

    fun calcularImpuesto(): Float {
        val subtotal = calcularSubtotal()
        return subtotal * (impuestoPorc / 100)
    }

    fun calcularTotal(): Float {
        val subtotal = calcularSubtotal()
        val impuesto = calcularImpuesto()
        return subtotal - impuesto
    }
}