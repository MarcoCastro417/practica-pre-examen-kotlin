package com.example.nomina_kotlin

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class ReciboNominaActivity : AppCompatActivity() {
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtHorasTrabNormales: EditText
    private lateinit var txtHorasTrabExtra: EditText
    private lateinit var rdbPuesto: RadioGroup
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var lblSubtotalTotal: TextView
    private lateinit var lblImpuestoTotal: TextView
    private lateinit var lblTotalTotal: TextView
    private lateinit var lblUsuario: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)
        iniciarComponentes()

        // Obtener los datos del MainActivity
        val datos = intent.extras
        val nombre = datos?.getString("nombre")
        lblUsuario.text = nombre

        btnCalcular.setOnClickListener {
            if (validarCampos()) {
                calcularNomina()
            } else {
                mostrarToast("Por favor, completa todos los campos")
            }
        }

        btnLimpiar.setOnClickListener {
            limpiarCampos()
        }

        btnRegresar.setOnClickListener {
            regresar()
        }
    }

    private fun iniciarComponentes() {
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasTrabNormales = findViewById(R.id.txtHorasTrabNormales)
        txtHorasTrabExtra = findViewById(R.id.txtHorasTrabExtra)
        rdbPuesto = findViewById(R.id.rdbPuesto)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        lblSubtotalTotal = findViewById(R.id.lblSubtotalTotal)
        lblImpuestoTotal = findViewById(R.id.lblImpuestoTotal)
        lblTotalTotal = findViewById(R.id.lblTotalTotal)
        lblUsuario = findViewById(R.id.lblUsuario)
    }

    private fun validarCampos(): Boolean {
        val numRecibo = txtNumRecibo.text.toString()
        val nombre = txtNombre.text.toString()
        val horasNormales = txtHorasTrabNormales.text.toString()
        val horasExtra = txtHorasTrabExtra.text.toString()
        val selectedRadioButton = findViewById<RadioButton>(rdbPuesto.checkedRadioButtonId)

        return numRecibo.isNotEmpty() && nombre.isNotEmpty() && horasNormales.isNotEmpty() && horasExtra.isNotEmpty() && selectedRadioButton != null
    }

    private fun calcularNomina() {
        // Obtener los valores de entrada de los EditTexts
        val numRecibo = txtNumRecibo.text.toString().toInt()
        val nombre = txtNombre.text.toString()
        val horasNormales = txtHorasTrabNormales.text.toString().toFloat()
        val horasExtra = txtHorasTrabExtra.text.toString().toFloat()

        // Obtener el texto del radio button seleccionado
        val selectedRadioButton = findViewById<RadioButton>(rdbPuesto.checkedRadioButtonId)
        var puesto = 0

        if (selectedRadioButton != null) {
            val puestoText = selectedRadioButton.text.toString()

            when {
                puestoText.equals("Auxiliar", ignoreCase = true) -> puesto = 1
                puestoText.equals("Albañil", ignoreCase = true) -> puesto = 2
                puestoText.equals("Ing Obra", ignoreCase = true) -> puesto = 3
            }
        }

        val reciboNomina = ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto, 16f)

        // Realizar los cálculos necesarios en el objeto reciboNomina
        val subtotal = reciboNomina.calcularSubtotal()
        val impuesto = reciboNomina.calcularImpuesto()
        val total = reciboNomina.calcularTotal()

        // Mostrar los valores calculados en los TextViews
        lblSubtotalTotal.text = subtotal.toString()
        lblImpuestoTotal.text = impuesto.toString()
        lblTotalTotal.text = total.toString()
    }

    private fun limpiarCampos() {
        txtNumRecibo.setText("")
        txtNombre.setText("")
        txtHorasTrabNormales.setText("")
        txtHorasTrabExtra.setText("")
        rdbPuesto.clearCheck()
        lblSubtotalTotal.text = ""
        lblImpuestoTotal.text = ""
        lblTotalTotal.text = ""
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Regresar al MainActivity?")
        confirmar.setPositiveButton("Confirmar") { dialog, _ ->
            finish()
        }
        confirmar.setNegativeButton("Cancelar") { dialog, _ ->
            dialog.dismiss()
        }
        confirmar.show()
    }

    private fun mostrarToast(mensaje: String) {
        Toast.makeText(applicationContext, mensaje, Toast.LENGTH_SHORT).show()
    }
}